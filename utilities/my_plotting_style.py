#!/usr/bin/env python3
"""
This should improve the graphics in the document by using TeX for text,
(if high chosen) using the same font as the report, and using reasonable text sizes.
Usage:
from my_plotting_style import mps
mps.mps_defaults('high) # if you want to engage TeX.
Incurs a large time penalty, worth it only for final plots.
Blank entries should cause plots to inherit fonts from the document.
"""

import numpy as np
import matplotlib
import matplotlib.style
import matplotlib.colors as colors
import matplotlib.pyplot as plt


def mps_defaults(quality='high'):
    """
    Apply my plotting style to produce nice looking figures.
    Call this at the start of a script which uses matplotlib,
    and choose the correct setting.
    :return:
    """
    # matplotlib.use('agg')
    if quality == 'high':
        matplotlib.style.use('seaborn-colorblind')
        p_setting = {"pgf.texsystem": "pdflatex",
                     "text.usetex": True,
                     "font.family": "serif",
                     "font.serif": [],
                     "font.sans-serif": ["DejaVu Sans"],    # gets rid of error messages
                     "font.monospace": [],
                     "lines.linewidth": 0.75,
                     "axes.labelsize": 10,
                     "font.size": 6,
                     "legend.fontsize": 8,
                     "xtick.labelsize": 10,
                     "ytick.labelsize": 10,
                     "image.cmap": 'RdYlBu_r',
                     # I think that this is a good temperature cmap
                     "animation.html": 'none',
                      "animation.writer": 'ffmpeg',          # MovieWriter 'backend' to use
                     "animation.codec": 'h264',             # Codec to use for writing movie
                     "animation.bitrate": -1,               # -1 implies let utility auto-determine
                     # Controls size/quality trade off for movie.
                     "animation.frame_format": 'png',
                     # Controls frame format used by temp files
                     "animation.ffmpeg_path": 'ffmpeg',
                     # Path to ffmpeg binary. Without full path $PATH is searched
                     "animation.ffmpeg_args": '',           # Additional arguments to pass to ffmpeg
                     "animation.avconv_path": 'avconv',
                     # Path to avconv binary. Without full path $PATH is searched
                     "animation.avconv_args": '',           # Additional arguments to pass to avconv
                     "pgf.preamble": [r"\usepackage[utf8x]{inputenc} \usepackage[T1]{fontenc} \usepackage[separate -uncertainty=true]{siunitx}"]
                     }
    else:
        matplotlib.style.use('seaborn-colorblind')
        p_setting = {"text.usetex": False,
                     "lines.linewidth": 0.75,
                     "axes.labelsize": 10,
                     "font.size": 6,
                     "legend.fontsize": 8,
                     "xtick.labelsize": 10,
                     "ytick.labelsize": 10,
                     "image.cmap": 'RdYlBu_r',
                     }

    matplotlib.rcParams.update(p_setting)


def save_fig_twice(name):
    """
    :param fig:
    :return:
    """
    plt.tight_layout()
    plt.savefig(name + '.png', dpi=600,  bbox_inches='tight')
    plt.savefig(name + '.pdf', bbox_inches='tight')


def single_column_width(fig):
    """Legacy function"""
    fig = defined_size(fig, size='single_column')
    return fig


def defined_size(fig, size='single_column'):
    """
    #---text sizes are meaningless if LaTeX squashes figure---
    usage:
    fig = plt.gcf()
    fig = mps.defined_size(fig, size='double_column_square')
    plt.tight_layout()
    ######
    Sizes are in inches by default.
    Measurements taken by ruler from a report.
    :param fig: the figure to be resized
    :param size: key word argument to get the right
    :param kwargs: unnecesary.
    :return: figure with the correct sized boundary
    """
    size_dict = {'single_column': [3.125, 2.5],
                 'sc': [3.125, 2.5],
                 'scs': [3.125, 3.125],
                 'double_column_short': [6.5, 2.5],
                 'dcsh': [6.5, 2.5],
                 'dcm': [6.5, 4.5],
                 'double_column_square': [6.5, 6.5],
                 'dcsq': [6.5, 6.5],
                 'gal_an_size': [7, 6.5],
                 'scl': [3.125, 6.5]}
    fig.set_size_inches(size_dict[size])
    return fig


class MidpointNormalize(colors.Normalize):
    """
    This class is useful for making color maps range between particular
    values. Was used in the bathymetry BSOSE map.
    """
    def __init__(self, vmin=None, vmax=None, vcenter=None, clip=False):
        """
        Initialise the color map as approximately the same as the normal
        matplotlib normalising function.
        :param vmin:
        :param vmax:
        :param vcenter:
        :param clip:
        """
        self.vcenter = vcenter
        colors.Normalize.__init__(self, vmin, vmax, clip)

    def __call__(self, value, clip=None):
        """
        This function apparently over rides the python class default.
        :param value:
        :param clip:
        :return:
        """
        # I'm ignoring masked values and all kinds of edge cases to make a
        # simple example...
        x_value, y_value = [self.vmin, self.vcenter, self.vmax], [0, 0.5, 1]
        return np.ma.masked_array(np.interp(value, x_value, y_value))


def _fading_colormap_name(from_name, fade_to_white=True):
    """
    Takes a python color name and returns a fading color map.
    :param from_name:
    :return:
    """
    red, green, blue, alpha = colors.to_rgba(from_name)

    return _fading_colormap_rgb((red, green, blue), fade_to_white=fade_to_white)


def _fading_colormap_hex(from_hex, fade_to_white=True):
    """
    Takes a hex string as input and returns a fading color map as output.
    :param from_hex:
    :return:
    """
    hex_number = from_hex.lstrip('#')
    return _fading_colormap_rgb(tuple(int(hex_number[i:i + 2], 16) for i in (0, 2, 4)),
                                fade_to_white=fade_to_white)


def _fading_colormap_rgb(from_rgb, fade_to_white=True):
    """
    Takes an r g b tuple and returns a fading color map.
    :param from_rgb: an r g b tuple
    :return:
    """

    # from color r,g,b
    red1, green1, blue1 = from_rgb

    # to color r,g,b
    red2, green2, blue2 = 1, 1, 1

    if fade_to_white:
        cdict = {'red': ((0, red1, red1),
                         (1, red2, red2)),
                 'green': ((0, green1, green1),
                           (1, green2, green2)),
                 'blue': ((0, blue1, blue1),
                          (1, blue2, blue2))}
    else:
        cdict = {'red': ((0, red2, red2),
                         (1, red1, red1)),
                 'green': ((0, green2, green2),
                           (1, green1, green1)),
                 'blue': ((0, blue2, blue2),
                          (1, blue1, blue1))}

    cmap = colors.LinearSegmentedColormap('custom_cmap', cdict)

    return cmap


def fading_colormap(from_color, fade_to_white=True):
    """
    Takes a hex or color name, and returns a fading color map.
    example usage:

    # cmap_a = fading_colormap('blue')
    # cmap_b = fading_colormap('#96f97b')

    :param from_color: either a hex or a name
    :return: cmap --> a colormap that can be used as a parameter in a plot.
    """
    if from_color.startswith('#'):
        cmap = _fading_colormap_hex(from_color,
                                    fade_to_white=fade_to_white)
    else:
        cmap = _fading_colormap_name(from_color,
                                     fade_to_white=fade_to_white)
    return cmap


def replacement_color_list(number_of_colors):
    """

    :param number_of_colors:
    :return:
    """
    assert isinstance(number_of_colors, int)

    color_d = {2: ['b', 'r'],
               3: ['b', 'green', 'r'],
               4: ['b', 'green', 'orange', 'r'],
               5: ['navy', 'b', 'green', 'orange', 'r'],
               6: ['navy', 'b', 'green', 'orange', 'r', 'darkred'],
               7: ['navy', 'b', 'green', '#d2bd0a', 'orange', 'r', 'darkred'],
               8: ['navy', 'b', 'green', '#d2bd0a', 'orange',
                   'r', 'darkred', '#fe019a'],
               9: ['navy', 'b', '#06c2ac', 'green', '#d2bd0a',
                   'orange', 'r', 'darkred', '#fe019a'],
               10: ['navy', 'b', '#06c2ac', 'green', '#82a67d',
                    '#d2bd0a', 'orange', 'r', 'darkred', '#fe019a'],
               11: ['navy', 'b', '#06c2ac', 'green', '#82a67d',
                    '#d2bd0a', 'orange', 'r', '#cf0234', 'darkred',
                    '#fe019a'],
               12: ['navy', 'b', '#06c2ac', 'green', '#82a67d', '#d2bd0a',
                    'orange', 'r', '#cf0234', 'darkred', '#6e1005', '#fe019a'],
               13: ['navy', 'b', '#06c2ac', 'green', '#82a67d',
                    '#d2bd0a', '#fdb915', 'orange', 'r', '#cf0234',
                    'darkred', '#6e1005', '#fe019a'],
               }
    if number_of_colors in color_d:
        color_list = color_d[number_of_colors]
    else:
        color_list = color_d[13]
    return color_list

