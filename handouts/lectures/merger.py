# import os
from PyPDF2 import PdfFileMerger
# file_list = os.listdir()
# print(file_list)
# file_list.sort()
# file_list.reverse()
# print(file_list)
x = ['2019-20_Pt_III_Minor_ASM_Handout_Part_III_lecture'+str(a)+'.pdf' for a in range(1,15)]
print(x)

merger = PdfFileMerger()

for pdf in x:
    merger.append(open(pdf, 'rb'))

with open("all_lectures.pdf", "wb") as fout:
    merger.write(fout)

