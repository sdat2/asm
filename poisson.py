"""
https://docs.scipy.org/doc/numpy-1.14.0/reference/generated/numpy.random.poisson.html
Weisstein, Eric W. “Poisson Distribution.” From MathWorld–A Wolfram Web Resource.
 http://mathworld.wolfram.com/PoissonDistribution.html

f(k ; \lambda)=\frac{\lambda^{k} e^{-\lambda}}{k !}

"""
import matplotlib.pyplot as plt
import numpy as np
import utilities.my_plotting_style as mps
mps.mps_defaults()

data = np.random.poisson(5, 100000)

d = np.diff(np.unique(data)).min()
left_of_first_bin = data.min() - float(d)/2
right_of_last_bin = data.max() + float(d)/2
plt.hist(data, np.arange(left_of_first_bin, right_of_last_bin + d, d), color='pink', normed=True)
plt.xlabel('Number of Occurances of Value')
plt.ylabel('Frequency')

plt.show()

# s = np.random.poisson(lam=(100., 500.), size=(100, 2))
