"""

"""
# Python code for the plot

import numpy as np
import matplotlib.pyplot as plt
import utilities.my_plotting_style as mps
mps.mps_defaults()


def plot_geometric_brownian_motion():
    """
    Code stolen from wikipedia
    https://en.wikipedia.org/wiki/Geometric_Brownian_motion
    d S_{t }= \mu S_{t} dt + \sigma S_{t} d W_{t}
    TODO In the wikipedia version of the plot the particles start above zero and then some approach it
    TODO where as in the current plot, all seem to start from zero.
    :return:
    """
    mu = 1
    n = 50
    dt = 0.1
    x0 = 100
    np.random.seed(1)

    # create different sigma values
    sigma = np.arange(0.4, 2, 0.1)

    # Computes the GBM initialisation.
    x = np.exp((mu - sigma ** 2 / 2) * dt
               + sigma * np.random.normal(0, np.sqrt(dt),
                                          size=(len(sigma), n)).T)
    x = np.vstack([np.ones(len(sigma)), x])
    x = x0 * x.cumprod(axis=0)

    # plots values. Should come close to solution:
    # S_{t}=S_{0} \exp \left(\left(\mu-\frac{\sigma^{2}}{2}\right) t+\sigma W_{t}\right)
    plt.plot(x)
    plt.legend(np.round(sigma, 2))
    plt.xlabel("Time, $t$ (s)")
    plt.ylabel("Distance, $x$ (m)")
    plt.title("Realizations of Geometric Brownian Motion with different variances\n $\mu=1$")
    plt.xlim(0, n)
    plt.show()


# plot_geometric_brownian_motion()


def stack_exchange_version():
    """
    https://stackoverflow.com/questions/13202799/python-code-geometric-brownian-motion-whats-wrong
    :return:
    """
    T = 2
    mu = 0.1
    S0 = 20
    dt = 0.01
    N = round(T / dt)
    t = np.linspace(0, T, N)

    def return_displacement(sigma=0.01):
        W = np.random.standard_normal(size=N)
        W = np.cumsum(W) * np.sqrt(dt)  ### standard brownian motion ###
        X = (mu - 0.5 * sigma ** 2) * t + sigma * W
        displacement = S0 * np.exp(X)  ### geometric brownian motion ###
        return displacement

    sigmas = [0.01, 0.005, 0.001, 0.0005]
    displacements = []
    for sigma in sigmas:
        displacements.append(return_displacement(sigma=sigma))

    for i in range(len(sigmas)):
        plt.plot(t, displacements[i], label='$\sigma = $'+str(sigmas[i]))
    plt.legend()
    plt.xlim(0, T)
    plt.title("Realizations of Geometric Brownian Motion with different variances\n $\mu=0.1$")

    plt.show()


stack_exchange_version()
