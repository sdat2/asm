"""
https://stackoverflow.com/questions/33600795/single-wiener-process-brownian-motion

"""
import numpy.random as npr
import numpy as np
import matplotlib.pyplot as plt
import utilities.my_plotting_style as mps
mps.mps_defaults()


def wiener1(time_increment=0.1,
            initial_value=10,
            number_timesteps=1000):
    """ Input variables:
    time_increment    time step
    initial_value    intial value, X(t=0) = initial_value
    number_timesteps    number of time steps
    """
    # create result array
    res = np.zeros(number_timesteps)
    # initialize start value
    res[0] = initial_value
    # print(res)
    # calculate and store time series
    for i in range(1, number_timesteps):
        res[i] = res[i - 1] + np.sqrt(time_increment) * npr.randn()

    # return time series
    return res


def wiener2(time_increment=0.1,
            initial_value=10,
            number_timesteps=1000):
    """ Input variables:
    time_increment    time step
    initial_value    intial value, X(t=0) = initial_value
    number_timesteps    number of time steps
    """
    # initialize start value
    res = initial_value
    # calculate process result
    for i in range(1, number_timesteps):
        res += np.sqrt(time_increment) * npr.randn()

    # return final value after t = N*time_increment
    return res


def test_and_plot():
    res1 = wiener1()
    # res2 = wiener2()
    plt.plot(res1)
    # plt.plot(res2)
    plt.xlabel('Time Steps')
    plt.ylabel('Displacement')
    plt.show()


test_and_plot()
