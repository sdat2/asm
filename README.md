# ASM

Advanced Statistical Mechanics:

The accompaniment my 4th year physics module. Scripts require LaTeX to run.

Suggested Exercises:

   1. Simulate GBM to show that at high $\sigma$ the value decreases.
   2. Investigate Mean First Passage Time (MFPT) with reflecting boundary conditions using
      either infinite sum of images
      or sum of discrete modes of differential equation. (MPFPT = L(L+2a)/2D)
   3. (Lecture 12) look at how the memory kernel changes for a mechanical model.
   11. Share prices in different markets
       Let us the price of a given share, S, that evolves in a market according to the Geometric Brownian Motion equation
       where $\mu$ is the expected rate of return, $\sigma$is the variance of market fluctuations,
       and Wt is the Wiener stochastic process with the zero mean and the variance.
       _a._ Follow the Ito calculus discussed in the lectures to confirm the solution of this SDE:
       _b._ Plot several realisations of this share price for a market with $\mu$=1, and $\sigma$=0.5, 1, 1.5, and 2.
      
  Course content:
  
  --- Introduction ---
  
   1. Revision of Equilibrium Stat Mech. L. 1-2
   
   2. Basic Stochastic variables, characteristic functions, CLT L.2
   
   --- Core ---
   
   3. Random walk-stochastic process, Markov Poisson process L.3
   
   4. Wiener Process: basic G.B.M., Black Scholes, Ornstein Uhlenbeck L.4-6
   
   5. Kramers-Moyal: Fokker Planck Discription L.7-8
   
   6. Mean First Passage Time L.9-10
   
   --- Extension ---
   
   7. Multiplicative noise, Itoh-Stratonovich (Wong Zakai) L.11
   
   8. Non-Markov, memory kernel, FDT, Caldeira Legget L.12-13
   
   9. Quantum Tunneling Crossover L.14
 